package se.kth.csc.iprog.agendabuilder;

import se.kth.csc.iprog.agendabuilder.R;
import android.os.Bundle;
import android.app.Activity;
import se.kth.csc.iprog.agendabuilder.model.AgendaModel;

public class AgendaMainActivity extends Activity {

	// Initializing variables
	private AgendaModel model;
	private AgendaMainView view;
	private AgendaMainController controller;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_agenda_main);
		
		model = ((AgendaBuilderApplication) getApplication()).getModel();
		controller = new AgendaMainController(this, model);
		view = new AgendaMainView(this, model, controller);
		
		model.addObserver(view);
	}

}






