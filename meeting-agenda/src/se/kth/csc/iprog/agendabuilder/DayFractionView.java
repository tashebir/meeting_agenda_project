package se.kth.csc.iprog.agendabuilder;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.LinearLayout;

/**
 * Wrapper for a LinearLayout, works the same but draws a red vertical line at 70% of the width
 */
public class DayFractionView extends LinearLayout {
	private Paint paint = new Paint();
	
	public DayFractionView(Context context) {
		super(context);
		paint.setColor(Color.RED);
		setWillNotDraw(false);
	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
		super.dispatchDraw(canvas);
		canvas.drawLine((float)getWidth() * 0.7f, 0, (float)getWidth() * 0.7f, getHeight(), paint);
	}
}
