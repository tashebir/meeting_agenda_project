package se.kth.csc.iprog.agendabuilder;

import android.app.Application;
import se.kth.csc.iprog.agendabuilder.model.AgendaModel;

public class AgendaBuilderApplication extends Application {
	private AgendaModel model = AgendaModel.getModelWithExampleData();
	private Object tempData;
	
	public AgendaModel getModel() {
		return model;
	}
	
	public void setTempData(Object o) {
		tempData = o;
	}
	
	public Object getTempData() {
		return tempData;
	}
}
