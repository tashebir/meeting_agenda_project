package se.kth.csc.iprog.agendabuilder;
import java.util.Observable;
import java.util.Observer;

import se.kth.csc.iprog.agendabuilder.model.AgendaModel;
import android.app.Activity;
import android.graphics.Color;
import android.text.InputType;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AgendaMainView implements Observer {
	
	private Activity activity;
	private AgendaModel model;
	private AgendaMainController controller;
	
	public AgendaMainView(Activity activity, AgendaModel model, AgendaMainController controller) {
		
		this.activity = activity;
		this.model = model;
		this.controller = controller;
		
		this.activity.findViewById(R.id.button_add_act).setOnClickListener(this.controller);
		this.activity.findViewById(R.id.button_add_day).setOnClickListener(this.controller);
		this.activity.findViewById(R.id.activity_list).setOnDragListener(this.controller);
		
		update(model, null);
	}

	
	@Override
	public void update(Observable observable, Object data) {
		LinearLayout activityList = (LinearLayout)activity.findViewById(R.id.activity_list);
		
		activityList.removeAllViews();
		controller.reset();
		int id = 1000;
		for (se.kth.csc.iprog.agendabuilder.model.Activity act : model.parkedActivites) {
			LinearLayout listEntry = new LinearLayout(activity);
			listEntry.setId(id);
			
			TextView lengthText = new TextView(activity);
			lengthText.setText(act.getLength() + " min");
			lengthText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			lengthText.setPadding(10, 10, 10, 10);
			listEntry.addView(lengthText);
			
			TextView nameText = new TextView(activity);
			nameText.setText(act.getName());
			switch (act.getType()) {
			case se.kth.csc.iprog.agendabuilder.model.Activity.PRESENTATION:
				nameText.setBackgroundColor(Color.rgb(90, 90, 220));
				break;
			case se.kth.csc.iprog.agendabuilder.model.Activity.GROUP_WORK:
				nameText.setBackgroundColor(Color.rgb(220, 90, 90));
				break;
			case se.kth.csc.iprog.agendabuilder.model.Activity.DISCUSSION:
				nameText.setBackgroundColor(Color.rgb(90, 220, 90));
				break;
			case se.kth.csc.iprog.agendabuilder.model.Activity.BREAK:
				nameText.setBackgroundColor(Color.rgb(220, 220, 90));
				break;
			}
			nameText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			nameText.setPadding(10, 10, 10, 10);
			listEntry.addView(nameText);

			listEntry.setOrientation(LinearLayout.HORIZONTAL);
			listEntry.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			activityList.addView(listEntry);
			
			controller.setDraggableActivity(listEntry, null, act);
			listEntry.setOnClickListener(controller);
			
			id++;
		}
		
		if (activityList.getChildCount() == 0)
			activityList.setLayoutParams(new LinearLayout.LayoutParams(350, 60));
		else
			activityList.setLayoutParams(new LinearLayout.LayoutParams(350, LinearLayout.LayoutParams.WRAP_CONTENT));
		
		String s = "";
		LinearLayout bigList = (LinearLayout)activity.findViewById(R.id.bigList);
		bigList.removeAllViews();
		
		int counter = 0;
		
		int dayId = 2000;
		for (se.kth.csc.iprog.agendabuilder.model.Day day : model.days) {
			
			counter++;
			
			LinearLayout listDay = new LinearLayout(activity);
			listDay.setId(dayId);
			controller.addDayDragTarget(listDay, day);
			
			// Empty Line
			LinearLayout emptyDay = new LinearLayout(activity);
			
			TextView emptyText = new TextView(activity);
			emptyText.setText("");
			emptyText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			emptyText.setPadding(10, 10, 10, 10);
			emptyDay.addView(emptyText);
			
			emptyDay.setOrientation(LinearLayout.HORIZONTAL);
			emptyDay.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			listDay.addView(emptyDay);
			
			// Fraction of activities
			
			LinearLayout fractionContainer = new LinearLayout(activity);
			DayFractionView fractionDay = new DayFractionView(activity);
			
			TextView fractionText = new TextView(activity);
			fractionText.setText("DAY "+counter+":");
			fractionText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			fractionText.setPadding(10, 10, 10, 10);
			fractionContainer.addView(fractionText);
			
			//ToDo -> coloured fractions
			
			if(day.getActivities().size() > 0) {
									
				// Presentation
				
				TextView act1Text = new TextView(activity);
				act1Text.setBackgroundColor(Color.rgb(90, 90, 220));
				act1Text.setLayoutParams(new LinearLayout.LayoutParams((int)((double)day.getLengthByType(se.kth.csc.iprog.agendabuilder.model.Activity.PRESENTATION)/day.getTotalLength() * 248.0), LinearLayout.LayoutParams.WRAP_CONTENT));
				fractionDay.addView(act1Text);
						
				// Group_Work
			
				TextView act2Text = new TextView(activity);
				act2Text.setBackgroundColor(Color.rgb(220, 90, 90));
				act2Text.setLayoutParams(new LinearLayout.LayoutParams((int)((double)day.getLengthByType(se.kth.csc.iprog.agendabuilder.model.Activity.GROUP_WORK)/day.getTotalLength() * 248.0), LinearLayout.LayoutParams.WRAP_CONTENT));
				fractionDay.addView(act2Text);
			
			
				// Discussion
			
				TextView act3Text = new TextView(activity);
				act3Text.setBackgroundColor(Color.rgb(90, 220, 90));
				act3Text.setLayoutParams(new LinearLayout.LayoutParams((int)((double)day.getLengthByType(se.kth.csc.iprog.agendabuilder.model.Activity.DISCUSSION)/day.getTotalLength() * 248.0), LinearLayout.LayoutParams.WRAP_CONTENT));
				fractionDay.addView(act3Text);
			
				// Break
				TextView act4Text = new TextView(activity);
				act4Text.setBackgroundColor(Color.rgb(220, 220, 90));
				act4Text.setLayoutParams(new LinearLayout.LayoutParams((int)((double)day.getLengthByType(se.kth.csc.iprog.agendabuilder.model.Activity.BREAK)/day.getTotalLength() * 248.0), LinearLayout.LayoutParams.WRAP_CONTENT));
				fractionDay.addView(act4Text);
				
				fractionDay.setOrientation(LinearLayout.HORIZONTAL);
				fractionDay.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
				fractionDay.setPadding(0, 10, 0, 10);
				fractionContainer.addView(fractionDay);
			}
			
			fractionContainer.setOrientation(LinearLayout.HORIZONTAL);
			fractionContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			fractionContainer.setPadding(10, 10, 10, 10);
			listDay.addView(fractionContainer);
			
			
			// Start time
			
			LinearLayout startDay = new LinearLayout(activity);
			
			TextView startText = new TextView(activity);
			startText.setText("Start time:  ");
			startText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			startText.setPadding(10, 10, 10, 10);
			startDay.addView(startText);
			
			EditText startTime = new EditText(activity);
			if((day.getStart()%60)<10)
				s = day.getStart()/60 + ":0" + day.getStart()%60;
			else
				s = day.getStart()/60 + ":" + day.getStart()%60;
			startTime.setText(s);
			startTime.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			startTime.setPadding(10, 10, 10, 10);
			startTime.setInputType(InputType.TYPE_DATETIME_VARIATION_TIME);
			startTime.addTextChangedListener(new StartTimeWatcher(startTime, day));
			startDay.addView(startTime);
			
			startDay.setOrientation(LinearLayout.HORIZONTAL);
			startDay.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			listDay.addView(startDay);
			
			// End time
			
			LinearLayout endDay = new LinearLayout(activity);
			
			TextView endText = new TextView(activity);
			endText.setText("End time:  ");
			endText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			endText.setPadding(10, 10, 10, 10);
			endDay.addView(endText);
			
			TextView endTime = new TextView(activity);
			
			if((day.getEnd()%60)<10)
				s = day.getEnd()/60 + ":0" + day.getEnd()%60;
			else
				s = day.getEnd()/60 + ":" + day.getEnd()%60;
			endTime.setText(s);
			endTime.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			endTime.setPadding(10, 10, 10, 10);
			endDay.addView(endTime);
			
			endDay.setOrientation(LinearLayout.HORIZONTAL);
			endDay.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			listDay.addView(endDay);
			
			// Length
			
			LinearLayout lengthDay = new LinearLayout(activity);
			
			TextView lengthText = new TextView(activity);
			lengthText.setText("Total :  ");
			lengthText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			lengthText.setPadding(10, 10, 10, 10);
			lengthDay.addView(lengthText);
			
			TextView lengthT = new TextView(activity);
			lengthT.setText(day.getTotalLength()+" min");
			lengthT.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			lengthT.setPadding(10, 10, 10, 10);
			lengthDay.addView(lengthT);
			
			lengthDay.setOrientation(LinearLayout.HORIZONTAL);
			lengthDay.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			listDay.addView(lengthDay);
			
			// Loop on different activities of the day
			
			int duration = day.getStart();
			id = dayId + 100;
			for (se.kth.csc.iprog.agendabuilder.model.Activity act : day.getActivities()) {
				LinearLayout listEntry = new LinearLayout(activity);
				listEntry.setId(id);
				
				TextView startText2 = new TextView(activity);
				if((duration%60)<10)
					s = duration/60 + ":0" + duration%60;
				else
					s = duration/60 + ":" + duration%60;
				startText2.setText(s);
				startText2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
				startText2.setPadding(10, 10, 10, 10);
				listEntry.addView(startText2);
				
				duration += act.getLength();
				
				TextView nameText = new TextView(activity);
				nameText.setText(act.getName());
				switch (act.getType()) {
				case se.kth.csc.iprog.agendabuilder.model.Activity.PRESENTATION:
					nameText.setBackgroundColor(Color.rgb(90, 90, 220));
					break;
				case se.kth.csc.iprog.agendabuilder.model.Activity.GROUP_WORK:
					nameText.setBackgroundColor(Color.rgb(220, 90, 90));
					break;
				case se.kth.csc.iprog.agendabuilder.model.Activity.DISCUSSION:
					nameText.setBackgroundColor(Color.rgb(90, 220, 90));
					break;
				case se.kth.csc.iprog.agendabuilder.model.Activity.BREAK:
					nameText.setBackgroundColor(Color.rgb(220, 220, 90));
					break;
				}
				nameText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
				nameText.setPadding(10, 10, 10, 10);
				listEntry.addView(nameText);

				listEntry.setOrientation(LinearLayout.HORIZONTAL);
				listEntry.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
				listDay.addView(listEntry);
				
				controller.setDraggableActivity(listEntry, day, act);
				listEntry.setOnClickListener(controller);
				
				id++;
			}

			listDay.setOrientation(LinearLayout.VERTICAL);
			listDay.setLayoutParams(new LinearLayout.LayoutParams(350, LinearLayout.LayoutParams.WRAP_CONTENT));
			
			bigList.addView(listDay);
			
			dayId += 1000;
		}
	}
		
}
