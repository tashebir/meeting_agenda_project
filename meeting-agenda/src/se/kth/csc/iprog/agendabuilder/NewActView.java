package se.kth.csc.iprog.agendabuilder;
import java.util.Observable;
import java.util.Observer;

import se.kth.csc.iprog.agendabuilder.model.AgendaModel;
import android.app.Activity;
import android.widget.EditText;
import android.widget.Spinner;

public class NewActView implements Observer {
	
	private Activity activity;
	@SuppressWarnings("unused")
	private AgendaModel model;
	private NewActController controller;
	
	public NewActView(Activity activity, AgendaModel model, NewActController controller) {
		
		this.activity = activity;
		this.model = model;
		this.controller = controller;
		
		this.activity.findViewById(R.id.button_cancel_act).setOnClickListener(this.controller);
		this.activity.findViewById(R.id.button_save_act).setOnClickListener(this.controller);
		
		// Check if we received an activity (ie we are editing an activity)
		AgendaBuilderApplication app = ((AgendaBuilderApplication)this.activity.getApplication());
		Object data = app.getTempData(); 
		if (data instanceof se.kth.csc.iprog.agendabuilder.model.Activity) {
			// Update the view
			se.kth.csc.iprog.agendabuilder.model.Activity act = (se.kth.csc.iprog.agendabuilder.model.Activity)data;
			((EditText)this.activity.findViewById(R.id.edit_name)).setText(act.getName());
			((EditText)this.activity.findViewById(R.id.edit_description)).setText(act.getDescription());
			((EditText)this.activity.findViewById(R.id.edit_length)).setText(""+act.getLength());
			
			String[] types = activity.getResources().getStringArray(R.array.activity_types);
			int i = 0;
			for (String type : types) {
				if (controller.typeStrToInt.get(type) == act.getType())
					break;
				i++;
			}
			((Spinner)this.activity.findViewById(R.id.spinner_type)).setSelection(i);
			
			// Pass to controller
			this.controller.setAct((se.kth.csc.iprog.agendabuilder.model.Activity)data);
			
			app.setTempData(null);
		}
	}

	@Override
	public void update(Observable observable, Object data) {
			
	}
		
}