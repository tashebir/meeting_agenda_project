package se.kth.csc.iprog.agendabuilder;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.Color;
//import android.util.Log;
import android.util.SparseArray;
import android.view.DragEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import se.kth.csc.iprog.agendabuilder.model.AgendaModel;
import se.kth.csc.iprog.agendabuilder.model.Day;

public class AgendaMainController implements OnClickListener, View.OnDragListener, View.OnLongClickListener {
	private AgendaMainActivity activity;
	private AgendaModel model;
	
	/** Maps view ID to an activity */
	private SparseArray<se.kth.csc.iprog.agendabuilder.model.Activity> dragData = new SparseArray<se.kth.csc.iprog.agendabuilder.model.Activity>();
	
	/** Maps a view ID (representing an activity) to a day it originated from */
	private SparseArray<Day> dragDaysFrom = new SparseArray<Day>();
	
	/** Maps a view ID (representing a day in the list) to a day object */
	private SparseArray<Day> dragDayTargets = new SparseArray<Day>();
	
	public AgendaMainController(AgendaMainActivity activity, AgendaModel model) {
		this.activity = activity;
		this.model = model;
	}

	public void reset() {
		dragData.clear();
		dragDaysFrom.clear();
		dragDayTargets.clear();
	}
	
	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.button_add_act) {
			Intent intent = new Intent(this.activity.getBaseContext(), NewActActivity.class);
			this.activity.startActivity(intent);
		} else if (v.getId() == R.id.button_add_day) {
			model.addDay(8, 0);
		} else {
			// Clicking on an activity
			Intent intent = new Intent(this.activity.getBaseContext(), NewActActivity.class);
			
			se.kth.csc.iprog.agendabuilder.model.Activity act = dragData.get(v.getId());
			if (act != null)
				((AgendaBuilderApplication)this.activity.getApplication()).setTempData(act);
			
			this.activity.startActivity(intent);
		}
		
	}
	
	public void setDraggableActivity(View v, Day from, se.kth.csc.iprog.agendabuilder.model.Activity act) {
		dragData.put(v.getId(), act);
		dragDaysFrom.put(v.getId(), from);
		if (from != null) {
			v.setOnDragListener(this);
		}
		v.setOnLongClickListener(this);
	}

	@Override
	public boolean onDrag(View v, DragEvent event) {
		int action = event.getAction();
		
		switch (action) {
		case DragEvent.ACTION_DRAG_STARTED:
			v.setBackgroundColor(Color.argb(128, 130, 240, 246)); 
			v.invalidate();
			return true;
			
		case DragEvent.ACTION_DRAG_ENTERED:
			v.setBackgroundColor(Color.argb(128, 247, 150, 70));
			v.invalidate();
			return true;
			
		case DragEvent.ACTION_DRAG_LOCATION:
			return true;
			
		case DragEvent.ACTION_DRAG_EXITED:
			v.setBackgroundColor(Color.argb(128, 130, 240, 246));
			v.invalidate();
			return true;
			
		case DragEvent.ACTION_DROP:
			// Do something with the data
			int id = Integer.parseInt(event.getClipData().getItemAt(0).getText().toString());
			se.kth.csc.iprog.agendabuilder.model.Activity act = dragData.get(id);
			//Log.d("AgendaMainController", "Dropped act " + act.getName() + " (id "+id+") on id " + v.getId());
			
			if (v.getId() == R.id.activity_list) {
				// Dropped on parked activities
				if (!model.parkedActivites.contains(act)) {
					// Was not parked previously, remove it from the day it was in
					Day from = dragDaysFrom.get(id);
					if (from != null) {
						from.removeActivity(act);
					}
					
					model.addParkedActivity(act);
				}
			} else {
				Day targetDay = dragDayTargets.get(v.getId());
				//Log.d("AgendaMainController", "Target day = " + targetDay);
				
				if (targetDay != null) {
					// We have a target day we want to drop on
					
					// Check if we dragged from a previous day
					Day from = dragDaysFrom.get(id);
					//Log.d("AgendaMainController", "From day = " + from);
					if (from != null) {
						// Remove from previous day
						from.removeActivity(act);
					} else {
						// Remove from parked
						model.parkedActivites.remove(act);
					}
					targetDay.addActivity(act);
				} else {
					// We did not have a target day, check if we have a target activity instead
					se.kth.csc.iprog.agendabuilder.model.Activity targetAct = dragData.get(v.getId());
					if (targetAct != null) {
						// Get the indices of the target activity and the dragged activity
						
						Day from = dragDaysFrom.get(id);
						// Find which day the target is in
						Day targetActDay = null;
						int targetActIndex = 0;
						for (Day day : model.days) {
							if (day.getActivities().contains(targetAct)) {
								targetActDay = day;
								targetActIndex = day.getActivities().indexOf(targetAct);
								break;
							}
						}
						
						// Check if we dragged from parked
						if (from == null) {
							model.moveActivity(null, model.parkedActivites.indexOf(act), targetActDay, targetActIndex);
						} else {
							// Dragged from a day
							model.moveActivity(from, from.getActivities().indexOf(act), targetActDay, targetActIndex);
						}
					}
				}
			}
			
			v.setBackgroundColor(Color.TRANSPARENT);
			v.invalidate();
			return true;
			
		case DragEvent.ACTION_DRAG_ENDED:
			v.setBackgroundColor(Color.TRANSPARENT);
			v.invalidate();
			return true;
		}
		
		return false;
	}

	@Override
	public boolean onLongClick(View v) {
		ClipData clipData = ClipData.newPlainText("id", ""+v.getId());
		
		return v.startDrag(clipData, new View.DragShadowBuilder(v), null, 0);
	}

	public void addDayDragTarget(LinearLayout listDay, Day day) {
		dragDayTargets.put(listDay.getId(), day);
		listDay.setOnDragListener(this);
	}

}
