package se.kth.csc.iprog.agendabuilder;

import se.kth.csc.iprog.agendabuilder.model.Day;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class StartTimeWatcher implements TextWatcher {

	private EditText view;
	private Day day;
	
	public StartTimeWatcher(EditText v, Day d) {
		view = v;
		day = d;
	}
	
	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		view.setBackgroundColor(Color.TRANSPARENT);
		String text = s.toString();
		int hours = 8;
		int minutes = 0;
		boolean error = false;
		try {
			hours = Integer.parseInt(text.substring(0, text.indexOf(":")));
			minutes = Integer.parseInt(text.substring(text.indexOf(":") + 1));
			if (hours > 23 || hours < 0) error = true;
			if (minutes > 59 || minutes < 0) error = true;
		} catch (NumberFormatException e) {
			error = true;
		}
		
		if (error) {
			view.setBackgroundColor(Color.RED);
			return;
		}

		day.setStart(hours*60 + minutes);
	}

}
