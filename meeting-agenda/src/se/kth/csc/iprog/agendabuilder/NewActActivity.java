package se.kth.csc.iprog.agendabuilder;

import se.kth.csc.iprog.agendabuilder.model.AgendaModel;
import android.os.Bundle;
import android.app.Activity;

public class NewActActivity extends Activity {

	private AgendaModel model;
	@SuppressWarnings("unused")
	private NewActView view;
	private NewActController controller;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_act);
		

		model = ((AgendaBuilderApplication) getApplication()).getModel();
		controller = new NewActController(this, model);
		view = new NewActView(this, model, controller);
	}


}
