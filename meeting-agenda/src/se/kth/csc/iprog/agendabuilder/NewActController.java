package se.kth.csc.iprog.agendabuilder;

import java.util.HashMap;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Spinner;
import se.kth.csc.iprog.agendabuilder.model.AgendaModel;

public class NewActController implements OnClickListener {
	private Activity activity;
	private AgendaModel model;
	
	private se.kth.csc.iprog.agendabuilder.model.Activity act;
	
	protected final HashMap<String, Integer> typeStrToInt = new HashMap<String, Integer>(8);
	
	public NewActController(Activity activity, AgendaModel model) {
		this.activity = activity;
		this.model = model;
		
		String[] types = activity.getResources().getStringArray(R.array.activity_types);
		
		typeStrToInt.put(types[0], se.kth.csc.iprog.agendabuilder.model.Activity.PRESENTATION);
		typeStrToInt.put(types[1], se.kth.csc.iprog.agendabuilder.model.Activity.DISCUSSION);
		typeStrToInt.put(types[2], se.kth.csc.iprog.agendabuilder.model.Activity.GROUP_WORK);
		typeStrToInt.put(types[3], se.kth.csc.iprog.agendabuilder.model.Activity.BREAK);
		
	}

	public void setAct(se.kth.csc.iprog.agendabuilder.model.Activity act) {
		this.act = act;
	}
	
	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.button_cancel_act) {
			activity.finish();
		} else if (v.getId() == R.id.button_save_act) {
			// Create/edit the activity
			
			String chosenType = ((Spinner)activity.findViewById(R.id.spinner_type)).getSelectedItem().toString();
			
			if (act == null) {
				se.kth.csc.iprog.agendabuilder.model.Activity act =
						new se.kth.csc.iprog.agendabuilder.model.Activity(
								((EditText)activity.findViewById(R.id.edit_name)).getText().toString(),
								((EditText)activity.findViewById(R.id.edit_description)).getText().toString(),
								Integer.parseInt(((EditText)activity.findViewById(R.id.edit_length)).getText().toString()),
								(int)typeStrToInt.get(chosenType));
				model.addParkedActivity(act);
			} else {
				// Edit existing activity
				
				act.setName(((EditText)activity.findViewById(R.id.edit_name)).getText().toString());
				act.setDescription(((EditText)activity.findViewById(R.id.edit_description)).getText().toString());
				act.setLength(Integer.parseInt(((EditText)activity.findViewById(R.id.edit_length)).getText().toString()));
				act.setType((int)typeStrToInt.get(chosenType));
			}
			activity.finish();
		}
		
	}
}
